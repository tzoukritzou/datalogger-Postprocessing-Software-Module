import paho.mqtt.client as mqtt
import time

def on_connect(client, userdata, flags, rc):
	print("Connected with result code "+str(rc))
	#client.subscribe("topic1/")

def on_message(client, userdata, msg):
	print(msg.topic+" "+str(msg.payload))
	
	
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect("localhost", 1883, 60)
client.loop_start()

while True:
	client.publish("topic1", "Hello World")
	time.sleep(1)


