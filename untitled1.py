import paho.mqtt.client as mqtt
import MySQLdb
import time

db = MySQLdb.connect("localhost", "root", "170292", "monitor")
curs=db.cursor()


def on_connect(client, userdata, flags, rc):
	print("Connected with result code "+str(rc))
	client.subscribe("topic1/#")

def on_message(client, userdata, msg):
	print(msg.topic+" "+str(msg.payload))
	date = time.strftime('%Y-%m-%d')
	time1 = time.strftime('%H:%M:%S')
	curs.execute("INSERT INTO voltage (date, time, phaseA) VALUES (%s, %s, %s)",(date,time1,str(msg.payload)))
	db.commit()	

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect("localhost", 1883, 60)
client.loop_forever()
