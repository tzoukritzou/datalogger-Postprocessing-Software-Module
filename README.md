This repository develops all the software modules to interface with the smartRUE
datalogger device and the collected data.

 The collected data are delivered to the software module as a data stream,
 either through a buffered stream over a network connection (to be defined) or
 through an existing logfile.
 
 The core of the software modules process the data that are stored in a
 predefined format (to be defined), reformats them in the appropriate text 
 format as described in the IEC specification, and produces the required 
 metrics and statistics.
 
 The software module will ideally provide a visual interface for configuring and
 controlling the device, as well as a "real-time" monitor of the collected data.
 
 Technologies considered are MQTT protocol over lWIP.
 The software can be build in python, implementing an MQTT broker that will
 handle event logs from datalogger devices that act as MQTT clients subscribed
 to the afformentioned broker.
 
 This design seems to scale for all the possible interconnection scenarios,
 from one or multiple  datalogger connected to one local monitoring device, to 
 many dataloggers connected to one centralized monitor over the internet.
